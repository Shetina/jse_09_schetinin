package ru.t1.schetinin.tm.service;

import ru.t1.schetinin.tm.api.ICommandRepository;
import ru.t1.schetinin.tm.api.ICommandService;
import ru.t1.schetinin.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService (final ICommandRepository commandRepository){
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands(){
        return commandRepository.getCommands();
    }

}
